/**
 * @file include/define/size_t.h
 * Implementation of the C time_t specification.
 * @author Conlan Wesson
 */

#ifndef __INCLUDE_DEFINE_TIME_T_H_
#define __INCLUDE_DEFINE_TIME_T_H_

//! Used for time in seconds.
typedef unsigned long long time_t;

#endif // __INCLUDE_DEFINE_TIME_T_H_

