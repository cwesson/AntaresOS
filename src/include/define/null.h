/**
 * @file include/define/null.h
 * Implementation of the C NULL specification.
 * @author Conlan Wesson
 */

#ifndef __INCLUDE_DEFINE_NULL_H_
#define __INCLUDE_DEFINE_NULL_H_

#define NULL ((void*)0)    //!< Null pointer

#endif // __INCLUDE_DEFINE_NULL_H_

