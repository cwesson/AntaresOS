/**
 * @file include/define/size_t.h
 * Implementation of the C size_t specification.
 * @author Conlan Wesson
 */

#ifndef __INCLUDE_DEFINE_SIZE_T_H_
#define __INCLUDE_DEFINE_SIZE_T_H_

//! Unsigned integer type of the result of the sizeof operator.
typedef unsigned int size_t;

#endif // __INCLUDE_DEFINE_SIZE_T_H_

